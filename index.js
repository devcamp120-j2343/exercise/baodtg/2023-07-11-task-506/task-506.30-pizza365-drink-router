//Khai báo thư viện express:
const express = require('express');
const { drinkRouter } = require('./app/routes/drinkRouter');
const { voucherRouter } = require('./app/routes/voucherRouter');
const { userRouter } = require('./app/routes/userRouter');
const { orderRouter } = require('./app/routes/orderRouter');

//Khai báo app:
const app = express();

//Khai báo port:
const port = 8000;

app.use('/', drinkRouter)
app.use('/', voucherRouter)
app.use('/', userRouter)
app.use('/', orderRouter)

//Khởi động app:
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})